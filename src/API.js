const defaultConfig = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
};

const apiSettings = {

    fetchCustomerLogin: async data => {
        const endpoint = `http://test.solarc.pe/api/Customer/GetCustomerLogin`;
        return await (await fetch(endpoint,{
            method: 'post',
            headers: {
                'Content-Type': 'application/json'            
            },
            body: JSON.stringify(data)
          })).json();
    },

  };

export default apiSettings;

