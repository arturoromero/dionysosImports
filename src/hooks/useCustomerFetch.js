import { useEffect, useState } from "react";
import API from "../API";

export const useCustomerFetch = () => {

  const initialState = {
    messages: [], 
    data: [], 
    type: 0
  }

  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const data = {
    idLocal: 2,
    userId: "753482",
    userPass: "753482",
  };

  const fetchCustomerLogin = async () => {
    try {
      setError(false);
      setLoading(true);

      const CustomerLogin = await API.fetchCustomerLogin(data);
      setState((prev) => ({
        ...CustomerLogin,
      }));
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchCustomerLogin(1);
  }, []);

  return { state, loading, error };
};
