import Banner from '@components/Banner';
import Footer from '@components/Footer';
import Header from '@components/Header';
import { useAuth } from '@context/authContext';
import bannerCustomer from '@images/banner_customer.jpg';
import { TabPanelUnstyled } from '@mui/base';
import { styled } from '@mui/system';
import '@styles/CustomerPage.scss';
import '@styles/ProductsPage.scss';
import React from 'react';
import TabsProductsCustom from '../components/TabsProductsCustom';
import { useCustomerFetch } from '../hooks/useCustomerFetch';



const TabPanel = styled(TabPanelUnstyled)`
  width: 100%;
  font-size: 14px;
  display: flex;
`;

const Customer = () => {
  const { user, logout } = useAuth()

  const handleLogout = async () => {
    await logout();
  }

  const { state, loading, error} = useCustomerFetch();

  console.log(state.data[0]);

  const tabsCustomer = ["Profile", "History", "Account Payables"];

  return (
    <>
      <Header />
      <Banner srcBanner={bannerCustomer} title='Customer' />
      <div className='loginContainer'>
        <div className='loginButtonCustomer'>
            {!user && (
              <Button text='Login' icon='fa-solid fa-unlock-keyhole' url='/login' />
            )}
        </div>
      </div>
      <div className='containerCustomer'>
        <Filters filterTitle='Filter Customers'>
          <Select label='City' />
          <Select label='State' />
          <Select label='Type' />
        </Filters>
        <Divider className='dividerTabs' orientation="vertical" flexItem />
        <CardProducts 
          imageUrl={distinctiveImage} 
          title="Distinctive Imports, Inc."
          code="941 Estes Court,"
          country="Schaumburg, IL 60193"
          type="Tel: (312) 456-5597"
          size="Distribution in Illinois"
        />
      </div>
      <>
        <TabsProductsCustom tabs={tabsCustomer}>
          <TabPanel value={0}>
            <div style={{margin: "20px"}}>{state.data[0]? state.data[0].custName:null }</div>

            <div style={{margin: "20px"}}>{state.data[0]? state.data[0].custNum:null }</div>
           
            <div style={{margin: "20px"}}>{state.data[0]? state.data[0].idCustomer:null }</div>
          </TabPanel>
        </TabsProductsCustom>
      </>
      <Footer />
    </>
  )
}

export default Customer